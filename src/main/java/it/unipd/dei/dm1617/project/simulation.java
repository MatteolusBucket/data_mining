package it.unipd.dei.dm1617.project;

import it.unipd.dei.dm1617.InputOutput;
import it.unipd.dei.dm1617.WikiPage;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.feature.Word2VecModel;
import org.apache.spark.mllib.linalg.Vector;

import java.util.ArrayList;

/**
 * Created by root on 23/05/17.
 * General class for project simulation
 */
public class simulation {

    public static void main(String[] args){

        String dataPath = args[0];    //cartella di lavoro
        String modelName = args[1];   //passare il nome della cartella del modello come parametro

        //spark context creation
        SparkConf conf = new SparkConf(true).setAppName("simulation");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Word2VecModel model = Word2VecModel.load(sc.sc(), "./models/" + modelName);

        JavaRDD<WikiPage> pages = (InputOutput.read(sc, dataPath));
        System.out.println("Input read");

        //call for word2vimport
        JavaPairRDD<WikiPage, Vector> pagesAndVectors = w2vimport.pairWikiVector(sc, model, pages, "./data/small/filteredLemmas80");

        //System.out.println(cluster_evaluation.hopkinsStatistic(sc,model,pagesAndVectors.values(),1));

        //CLUSTER
        //acquisire il modello del cluster
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        System.out.println("SIM: Simulazione 1 Kmeans con dataset reducted prova con 10 iterazioni e k che va da 5 a 30 con inc di 5" +
                "valutazioni con fractionization e silhouette random");

        System.out.println("SIM: Lunghezza dell'intero dataset = " + pagesAndVectors.collectAsMap().size());
        
        KMeansModel cluster_model;                  //modello del cluster
        JavaPairRDD<Integer,Vector> centroidrdd;    //centri del cluster
        double silPreciseCoeff;                 //coefficiente di silhouette precisa
        JavaPairRDD<Integer, ArrayList<Vector>> clusteredVector;
        JavaRDD<Vector> small_dataset;

        /*
        for(int k = 10; k <=100; k+=10 ){
            cluster_model = my_cluster.my_kmeans_model(pagesAndVectors, 10, k);
            centroidrdd = giveCentroid(sc, cluster_model.clusterCenters());
            clusteredVector = my_cluster.clustVector(pagesAndVectors, cluster_model);

            //silhouette random
            for(int i = 50; i < 700; i = i + 50){
                double avg = 0;
                for(int rip = 0; rip < 4; rip++){
                    small_dataset = randomized_reducted_dataset(sc, pagesAndVectors, i);
                    double df = cluster_evaluation.random_silhouette_coefficient(cluster_model, sc, small_dataset, centroidrdd);
                    System.out.println("SIM: k = "+ k + ":: iterazione numero "+  rip + " lunghezza = " + small_dataset.collect().size() + " value = " + df);
                    avg = avg + df;
                }
                System.out.println("SIM: k = "+ k + ":: average = "+ avg/4);

            }

            //silhouette precisa
            silPreciseCoeff =cluster_evaluation.silhouette_evaluation(sc, clusteredVector,centroidrdd);
            System.out.println("SIM: K = " + k + " Silhouette precisa coefficient = " + silPreciseCoeff);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        */

        //////////////////////////////////////////////////////////////////test////////////////////////////////////////

        JavaRDD<Vector> dataset = pagesAndVectors
                .map(x -> x._2());


        //for(int k = 200; k <=4000; k+=300 ){
            int k = 20;
            int percent = 10;
            cluster_model = my_cluster.my_kmeans_model(pagesAndVectors, 10, k);
            centroidrdd = giveCentroid(sc, cluster_model.clusterCenters());
            clusteredVector = my_cluster.clustVector(pagesAndVectors, cluster_model);

            //silhouette random
            //for(int percent = 10; percent <= 30; percent+=5){
                double avg = 0;
            //    for(int rip = 0; rip < 3; rip++){
                    double df = cluster_evaluation.random_percent_silhouette_coefficient(cluster_model, sc, dataset, centroidrdd, percent);
                    //System.out.println("SIM: k = "+ k + ":: iterazione numero "+  rip + " percentuale = " + percent + " value = " + df);
                    System.out.println("SIM: k = "+ k + " percentuale = " + percent + " value = " + df);
        //        avg+=df;
            //    }
            //    System.out.println("SIM: k = "+ k + ":: average = "+ avg/3);

            //}
            //silhouette precisa
            silPreciseCoeff =cluster_evaluation.silhouette_evaluation(sc, clusteredVector,centroidrdd);
            System.out.println("SIM: K = " + 20 + " Silhouette precisa coefficient = " + silPreciseCoeff);
        //}
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////


        //KMeansModel cluster_model = my_cluster.my_kmeans_model(pagesAndVectors, 10, 10);
        //BisectingKMeansModel cluster_model = my_cluster.my_bisectingkmeans_model(pagesAndVectors, 20, 10);

        //fornire il modello e tornare il JavaPairRDD con integer e arayList
        //JavaPairRDD<Integer, ArrayList<Vector>> clusteredVector = my_cluster.clustVector(pagesAndVectors, cluster_model);
        //System.out.println("Modello fornito");

        //libero memoria:
        //pages = null;

        //JavaPairRDD<Integer,Vector> centroidrdd = giveCentroid(sc, cluster_model.clusterCenters());


        //EVALUATION
        //metodo che gli passo modello e pagesAndVector e torna la silouette coefficient
        //double x = cluster_evaluation.silhouette_evaluation(sc, clusteredVector, centroidrdd);
        //System.out.println("silhouette coefficient complete = " + x);
        //System.out.println("silhouette coefficient approximate = " + cluster_evaluation.silhouette_evaluation_aprox(sc,clusteredVector,centroidrdd));


        //prova del silhouette random
        //System.out.println("Lunghezza dell'intero dataset = " + pagesAndVectors.collectAsMap().size());

        //JavaRDD<Vector> small_dataset;

        //for(int i = 200; i < 800; i = i + 100){
          //  for(int rip = 0; rip < 4; rip++){
            //    small_dataset = randomized_reducted_dataset(sc, pagesAndVectors, i);
              //  System.out.println("Iterazione numero "+  rip + " lunghezza = " + small_dataset.collect().size() + " value = " + cluster_evaluation.random_silhouette_coefficient(cluster_model, sc, small_dataset, centroidrdd));
           // }
        //}

        //double silPreciseCoeff =cluster_evaluation.silhouette_evaluation(sc, clusteredVector,centroidrdd);
        //System.out.println("Silhouette precisa coefficient = " + silPreciseCoeff);

    }

    /**
     * Questo metodo prende dei vettori random dall'intero dataset
     * @param sc
     * @param pagesAndVectors
     * @param numb_reduct_vector
     * @return
     */
    private static JavaRDD<Vector> randomized_reducted_dataset(JavaSparkContext sc, JavaPairRDD<WikiPage, Vector> pagesAndVectors, int numb_reduct_vector){

        JavaRDD<Vector> Vectors = pagesAndVectors
                .map(x -> x._2());

        JavaRDD<Vector> smallVectorSample = sc.parallelize(Vectors.takeSample(false, numb_reduct_vector));

        return smallVectorSample;
    }

    private static JavaPairRDD<Integer,Vector> giveCentroid(JavaSparkContext sc, Vector[] centroids){

        ArrayList<Vector> aCentroids = new ArrayList<Vector>(centroids.length);
        ArrayList<Integer> clst_id = new ArrayList<Integer>(centroids.length);
        for(int i=0; i<centroids.length; i++) {
            aCentroids.add(i,centroids[i]);
            clst_id.add(i,i);
        }

        return sc.parallelize(clst_id).zip(sc.parallelize(aCentroids));
    }

}
