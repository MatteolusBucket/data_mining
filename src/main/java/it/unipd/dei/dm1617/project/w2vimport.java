package it.unipd.dei.dm1617.project;

import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;
import it.unipd.dei.dm1617.Distance;
import it.unipd.dei.dm1617.InputOutput;
import it.unipd.dei.dm1617.Lemmatizer;
import it.unipd.dei.dm1617.WikiPage;
import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;
import org.apache.avro.generic.GenericData;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.feature.Word2VecModel;

import org.apache.spark.mllib.linalg.BLAS;
import org.apache.spark.mllib.linalg.DenseVector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.sql.catalyst.expressions.aggregate.Collect;
import scala.Array;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import org.apache.spark.mllib.linalg.Vector;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by solda on 14/05/2017.
 */
public class w2vimport {
  private static final int VECTOR_DIMENSION = 1000;

  /**
   * Does the "king"-"man"+"woman" calculation ( should return queen in the first synonyms)
   * @param model
   */
  private static void testFunction(Word2VecModel model){
    scala.Tuple2<String,Object>[] lista = model.findSynonyms("king",5);
    System.out.println("--- Primi 5 sinonimi di king ---");
    for(int i=0; i<lista.length; i++)
      System.out.println(lista[i]._1()); //+ ":" + lista[i]._2());

    lista = model.findSynonyms("man",5);
    System.out.println("--- Primi 5 sinonimi di man ---");
    for(int i=0; i<lista.length; i++)
      System.out.println(lista[i]._1());

    lista = model.findSynonyms("woman",5);
    System.out.println("--- Primi 5 sinonimi di woman ---");
    for(int i=0; i<lista.length; i++)
      System.out.println(lista[i]._1());

    lista = model.findSynonyms("queen",5);
    System.out.println("--- Primi 5 sinonimi di queen ---");
    for(int i=0; i<lista.length; i++)
      System.out.println(lista[i]._1());

    Vector v = model.transform("king");
    BLAS.axpy(-1,model.transform("man"),v);
    BLAS.axpy(1,model.transform("woman"),v);
    lista = model.findSynonyms(v,5);
    System.out.println("\"king\" - \"man\" + \"woman\" = \"queen\" ?");
    for(int i=0; i<lista.length; i++)
      System.out.println(lista[i]._1());
  }

  /**
   *
   * @param sc JavaSparkContext in use
   * @param model Word2VecModel in use
   * @param pages pages to vectorize
   * @param lemmasPath path to pages lemmatized (data/medium/filteredLemmas)
   * @return a RDD of pairs of WikiPages,Vector
   */
  public static JavaPairRDD<WikiPage, Vector> pairWikiVector(JavaSparkContext sc, Word2VecModel model, JavaRDD<WikiPage> pages, String lemmasPath){

    JavaRDD<String> texts = word2vec.getTextFromWikiPages(pages);
    JavaRDD<ArrayList<String>> lemmas = word2vec.lemmatizeFilterAndSave(sc,texts,lemmasPath);
    JavaRDD<Vector> pageVec = vectorize(sc, lemmas, model).cache();


    JavaPairRDD<WikiPage, Vector> pagesAndVectors = pages.zip(pageVec)
            .filter((el) -> (el._2().numNonzeros() > 0)) // void vectors are deleted
            .cache();

    return pagesAndVectors;
  }


  /**
   * returns a JavaRDD of Vectors, representing the vector associated to the wikipage
   *
   * @param lemmas JavaRDD containing lists of words in the document, already lemmatized
   * @param model Word2VecModel to be used for the vectorization
   * @return JavaRDD of Vectors
   */
  public static JavaRDD<Vector> vectorize(JavaSparkContext sc, JavaRDD<ArrayList<String>> lemmas, Word2VecModel model) {
    Broadcast<Word2VecModel> brModel = sc.broadcast(model);
    JavaRDD<Vector> vec = lemmas.map((d) -> vectorize(d,brModel));

    return vec;
  }

  /**
   * returns a the Vector associated to the document, summing the vector representation for each lemma
   * and then scaling it by the number of lemmas in the document
   * @param lemmas list of words in the document, already lemmatized
   * @param model Word2VecModel to be used for the vectorization
   * @return Vector representing the document
   */
  public static Vector vectorize(ArrayList<String> lemmas, Broadcast<Word2VecModel> brModel){
    Word2VecModel model = brModel.getValue();
    Vector y = Vectors.zeros(VECTOR_DIMENSION);
    Vector x = Vectors.zeros(VECTOR_DIMENSION);
    int counter = 0;
    for(int i = 0; i<lemmas.size(); i++){
      if(i == 0)
        System.out.println("word:" + lemmas.get(i) + "::->" + model.transform(lemmas.get(i)));
      try{
        x = model.transform(lemmas.get(i));
        counter++;
        BLAS.axpy(1,x,y);
      }catch (IllegalStateException e) {}
    }

    if(counter>0)
      BLAS.scal((double)1/counter,y);
    System.out.println("vec num lemmas: " + counter + " with norm " + Vectors.norm(y,2));
    return y;
  }

  public static void main(String[] args) {

    String dataPath = args[0];
    String modelPath = args[1];
    String lemmasPath = args[2];

    SparkConf conf = new SparkConf(true).setAppName("word2vec");
    JavaSparkContext sc = new JavaSparkContext(conf);

    Word2VecModel model = Word2VecModel.load(sc.sc(), modelPath);
    // Load dataset of pages
    JavaRDD<WikiPage> pages = word2vec.getWikiPages(sc,dataPath);
    JavaPairRDD<WikiPage, Vector> pagesAndVectors = pairWikiVector(sc,model,pages,lemmasPath);

    System.out.println(cluster_evaluation.hopkinsStatistic(sc,model,pagesAndVectors.values(),1000));

    System.out.println("PUNTO 8");
    //pagesAndVectors.saveAsObjectFile("./data/pagesAndVectors");
    //pagesAndVectors.saveAsTextFile("./data/pagesAndVectors");


    //testFunction(model);
/*
    // Packs together wikipages and corresponding vectors, we can do it as we didn't change orders
    List<Tuple2<WikiPage, Vector>> firstPages = pagesAndVectors.take(2);
    System.out.println(firstPages.get(0)._1().getTitle() + " : " + firstPages.get(0)._1().getText() + " : " + firstPages.get(0)._2());
    System.out.println(firstPages.get(1)._1().getTitle() + " : " + firstPages.get(1)._1().getText() + " : " + firstPages.get(1)._2());
    double dist = Distance.cosineDistance(firstPages.get(0)._2(), firstPages.get(1)._2());
    //System.out.println("Cosine distance between `" +
    //        firstPages.get(0)._1().getTitle() + "` and `" +
    //        firstPages.get(1)._1().getTitle() + "` = " + dist);
*/
  }
}


