package it.unipd.dei.dm1617.project;

import it.unipd.dei.dm1617.InputOutput;
import it.unipd.dei.dm1617.Lemmatizer;
import it.unipd.dei.dm1617.WikiPage;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.feature.StopWordsRemover;
import org.apache.spark.mllib.feature.Word2Vec;
import org.apache.spark.mllib.feature.Word2VecModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by solda on 12/05/2017.
 */
public class word2vec {

  public static JavaRDD<WikiPage> getWikiPages(JavaSparkContext sc, String dataPath) {
    return InputOutput.read(sc, dataPath);
  }

  public static JavaRDD<String> getTextFromWikiPages(JavaRDD<WikiPage> pages){
    JavaRDD<String> texts = pages.map((p) -> p.getText());
    return texts;
  }

  public static JavaRDD<ArrayList<String>> lemmatizeFilterAndSave(JavaSparkContext sc, JavaRDD<String> texts, String lemmasPath){
    JavaRDD<ArrayList<String>> lemmas;
    try{
      lemmas = sc.objectFile(lemmasPath);
      lemmas.count();
      System.out.println("Lemmatization loaded from " + lemmasPath);
      return lemmas;
    }catch(Exception e){
      System.out.println(e);
      System.out.println("Lemmatization will be saved in " + lemmasPath);
    }
    lemmas = lemmatizeAndFilter(sc,texts);
    lemmas.saveAsObjectFile(lemmasPath);
    return lemmas;
  }

  public static JavaRDD<ArrayList<String>> lemmatizeAndFilter(JavaSparkContext sc, JavaRDD<String> texts){
    // Get the lemmas. It's better to cache this RDD since the
    // following operation, lemmatization, will go through it two
    // times.
    JavaRDD<ArrayList<String>> lemmas = Lemmatizer.lemmatize(texts).cache();


    Broadcast<Set<String>> bStopWords = sc.broadcast(
            new HashSet<>(Arrays.asList(StopWordsRemover.loadDefaultStopWords("english"))));

    JavaRDD<ArrayList<String>> filteredLemmas = lemmas.map((ls) -> {
      ArrayList<String> filtered = new ArrayList<>();
      for (String l : ls) {
        if (!(bStopWords.getValue().contains(l))) {
          filtered.add(l);
        }
      }
      return filtered;
    });

    return filteredLemmas;
  }

  public static Word2VecModel fitModel(JavaSparkContext sc, JavaRDD<ArrayList<String>> filteredLemmas, String modelPath, double learningRate){

    try{
      Word2VecModel wm = Word2VecModel.load(sc.sc(), modelPath);
      System.out.println("There was already a model in " + modelPath);
      return wm;
    }catch(Exception e){
      System.out.println("Fitting the model with VectorSize = 1000, Learning Rate = " + learningRate + ", minCount = 5");
    }

    // We create a Word2Vec model, setting the parameters to the default values
    Word2Vec w2v = new Word2Vec()
            .setMaxSentenceLength(1000)
            .setVectorSize(1000)
            .setLearningRate(learningRate)
            .setNumPartitions(8)
            .setNumIterations(1)
            .setWindowSize(5)
            .setMinCount(5);

    Word2VecModel w2vmodel = w2v.fit(filteredLemmas);
    w2vmodel.save(sc.sc(), modelPath);
    return w2vmodel;
  }


  public static void main(String[] args){
    String dataPath = args[0];
    String modelPath = args[1];
    String lemmasPath = args[2];

    // Usual setup
    SparkConf conf = new SparkConf(true).setAppName("word2vec");
    JavaSparkContext sc = new JavaSparkContext(conf);

    JavaRDD<WikiPage> pages = getWikiPages(sc, dataPath);
    // Get text out of pages
    JavaRDD<String> texts = getTextFromWikiPages(pages);
    System.out.println(texts.count());

    JavaRDD<ArrayList<String>> filteredLemmas = lemmatizeFilterAndSave(sc, texts, lemmasPath);

    Word2VecModel w2vmodel = fitModel(sc, filteredLemmas, modelPath, 0.007);

    /* DEBUG - LISTA LEMMA
    lemmas.foreach((l)-> {
      System.out.println(l.toString());
    });
    */

    scala.Tuple2<String,Object>[] lista = w2vmodel.findSynonyms("human",5);
    System.out.println("Sinonimi di human");
    for(int i=0; i<lista.length; i++)
      System.out.println(lista[i]._1());
    try{
      lista = w2vmodel.findSynonyms("and",5);
      System.out.println("Sinonimi di and");
      for(int i=0; i<lista.length; i++)
        System.out.println(lista[i]._1());
    } catch(Exception e){
      System.out.println("and assente");
    }
  }
}