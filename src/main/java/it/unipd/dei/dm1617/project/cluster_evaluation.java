package it.unipd.dei.dm1617.project;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import it.unipd.dei.dm1617.WikiPage;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.feature.Word2VecModel;
import org.apache.spark.mllib.linalg.BLAS;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import scala.Tuple2;

import java.io.Serializable;
import java.util.*;

/**
 * Created by root on 23/05/17.
 *
 */
public class cluster_evaluation {//todo
    private static final int VECTOR_DIMENSION = 1000;

    public static class DoubleComparator implements Comparator<Double>, Serializable {
        @Override
        public int compare(Double t1, Double t2) {
            return t1.compareTo(t2);
        }
    }

    public static double fraction_evaluation(Multimap<Object, WikiPage> wikiToDataset){
        //map: key = category and value = number of cluster in which it's mapped
        Multimap<String, Integer> cat_to_cluster = ArrayListMultimap.create();

        double fraction = 0; //average cluster fraction evaluation

        //all cluster evaluation
        for(Object cls_numb : wikiToDataset.keySet()) {
            //for each cluster

            for(WikiPage page : wikiToDataset.get(cls_numb)){
                //for each wikipage in this cluster

                //System.out.println("\nvaluto la pagina numero: " + page.getId());
                for(String cat : page.getCategories()){
                    //for each category of the wikipage
                    cat_to_cluster.put(cat, (Integer) cls_numb);
                }
            }
        }

        //for each category it evaluates the number of different associated clusters
        for(String cat : cat_to_cluster.keySet()){
            //System.out.println("\n evaluate la category: " + cat);

            Map<Integer, Integer> statistic = new HashMap<>();
            for (Integer element : cat_to_cluster.get(cat)){
                Integer count = 0;
                if (statistic.containsKey(element)){
                    count = statistic.get(element);
                }
                count++;
                statistic.put(element,count);
            }

            fraction = fraction + (1 / statistic.size());
        }

        int numb_categories = cat_to_cluster.keySet().size();
        fraction = fraction / numb_categories;

        //System.out.println("Number of category = " + numb_categories + ", percentage of fraction = " + fraction);
        return fraction;
    }


    public static double hopkinsStatistic(JavaSparkContext sc, Word2VecModel model, JavaRDD<Vector> vec, int n){

        JavaRDD<Vector> sample = sc.parallelize(vec.takeSample(false, n));
        ArrayList<Vector> rand = new ArrayList<>(n);

        Random randomizer= new Random();
        Vector tmp;
        scala.collection.immutable.IndexedSeq<Tuple2<String,Object>> modelVecs = model.wordIndex().toIndexedSeq();
        int modelSize = modelVecs.size();
        int randNumLemmas;

        // Random points creation
        for(int i=0; i<n; i++){
            randNumLemmas = randomizer.nextInt(1000) + 1;
            System.out.println("num lemmas: " + randNumLemmas);
            tmp = Vectors.zeros(VECTOR_DIMENSION);
            while(randNumLemmas-- > 0) {
                BLAS.axpy(1, model.transform(modelVecs.apply(randomizer.nextInt(modelSize))._1()), tmp);
                if(randNumLemmas%100 == 0)
                    System.out.println("partial rand point of norm: " + Vectors.norm(tmp,2));
            }
            System.out.println("rand point of norm: " + Vectors.norm(tmp,2));
            rand.add(tmp);
        }
        System.out.println("sample point of norm: " + Vectors.norm(sample.collect().get(0),2));
        System.out.println("rand point of norm: " + Vectors.norm(rand.get(0),2));
        JavaRDD<Vector> randomPoints = sc.parallelize(rand);

        Broadcast<List<Vector>> brVec = sc.broadcast(vec.collect());
        JavaRDD<Double> wi = sample
                .map((l)->{
                    System.out.println("3");
                    return new Double(distanceFromClosest(brVec,l, true));
                });
        double sumWi = wi.reduce((l1,l2)->{
            return l1.doubleValue()+l2.doubleValue();
        });

        JavaRDD<Double> ui = randomPoints
                .map((l)->{
                    return new Double(distanceFromClosest(brVec,l,false));
                });
        double sumUi = ui.reduce((l1,l2)->{
            return l1.doubleValue()+l2.doubleValue();
        });

        double result = sumWi/(sumUi+sumWi);
        System.out.println("result:" + result + "::" + sumUi + "::" + sumWi);
        return result;
    }

    /**
     *
     * @param brPageVecs list of all vectors of the dataset
     * @param vec vec of which we want to find the closest distance
     * @param isSample vec is a sample vector (true) or a random vector (false)
     * @return distance from the closest neighbour vector of the dataset
     */
    private static double distanceFromClosest(Broadcast<List<Vector>> brPageVecs, Vector vec, boolean isSample){
        /*if(isSample == true){
            return brPageVecs
                    .getValue()
                    .filter((l)->{
                        return !(l.equals(vec));
                    })
                    .map((l)->{
                        return Vectors.sqdist(l,vec);
                    })
                    .min(new DoubleComparator());
        } else{
            return brPageVecs
                    .getValue()
                    .map((l)->{
                        return Vectors.sqdist(l,vec);
                    })
                    .min(new DoubleComparator());
        }*/

        List<Vector> pageVecs = brPageVecs.getValue();
        double minDist = Double.POSITIVE_INFINITY;
        //System.out.println("Size:" + pageVecs.size());
        if(isSample){
            for(int i=0; i<pageVecs.size(); i++){
                if(!pageVecs.get(i).equals(vec))
                    if(Vectors.sqdist(pageVecs.get(i),vec) < minDist)
                        minDist = Vectors.sqdist(pageVecs.get(i),vec);
            }
        } else {
            for (int i = 0; i < pageVecs.size(); i++) {
                if (Vectors.sqdist(pageVecs.get(i), vec) < minDist)
                    minDist = Vectors.sqdist(pageVecs.get(i), vec);
            }
        }

        return minDist;

    }


    public static double silhouette_evaluation_aprox(JavaSparkContext sc, JavaPairRDD<Integer, ArrayList<Vector>> clusteredVector, JavaPairRDD<Integer, Vector> centroids){
        double sil_coef = 0;
        int counter = 0;
        long numb_clusters = clusteredVector.count();
        int percentage = 0;

        Broadcast<Map<Integer,Vector>> brCentroids = sc.broadcast(centroids.collectAsMap());
        //approccio per ogni cluster
        for (Integer clst_id : clusteredVector.keys().collect()){
            //lo faccio per tutti i vettori di quel cluster
            for (Vector v : clusteredVector.collectAsMap().get(clst_id)){
                sil_coef += one_silhouetteCoef(avg_dist_point_and_cluster_aprox(v, brCentroids.getValue().get(clst_id)), min_avg_dist_aprox(v, brCentroids, clst_id));
                counter++;
            }
            percentage++;
            System.out.println("COMPLETE: " + (percentage * 100) / numb_clusters);
        }

        return sil_coef/counter;
    }

    /**
     * Calculates the complete silhouette index for the given clustered dataset
     * @param sc javasparkcontext in use
     * @param clusteredVector clusters with associated vectors
     * @param centroids list of centroids of the clusters (not used)
     * @return silhouette coefficient
     */
    public static double silhouette_evaluation(JavaSparkContext sc, JavaPairRDD<Integer, ArrayList<Vector>> clusteredVector, JavaPairRDD<Integer, Vector> centroids){
        double sil_coef = 0;
        int counter = 0;
        long numb_clusters = clusteredVector.count();
        int percentage = 0;

        Broadcast<Integer> brClstID;
        Broadcast<Map<Integer,ArrayList<Vector>>> brClusteredVector = sc.broadcast(clusteredVector.collectAsMap());
        //approccio per ogni cluster
        for (Integer clst_id : clusteredVector.keys().collect()){
            //lo faccio per tutti i vettori di quel cluster
            brClstID = sc.broadcast(clst_id);
            for (Vector v : brClusteredVector.getValue().get(clst_id)){
                sil_coef += one_silhouetteCoef(avg_dist_point_and_cluster(v, brClusteredVector, brClstID), min_avg_dist(v, brClusteredVector, brClstID));
                counter++;
            }
            percentage++;
            System.out.println("COMPLETED CLUSTERS: " + (percentage * 100) / numb_clusters);
        }

        return sil_coef/counter;
    }

    /**
     * Calculates minimum average distance from a point and points of other clusters
     * @param point point to check
     * @param vectorToCluster map of clusters with associated vectors
     * @param cluster_id id of the cluster of which point is part
     * @return min avg distance from other clusters
     */
    private static double min_avg_dist(Vector point, Broadcast<Map<Integer, ArrayList<Vector>>> vectorToCluster, Broadcast<Integer> cluster_id){

        double min_dist = Double.POSITIVE_INFINITY;
        double local_avg_dst;
        for (Integer key : vectorToCluster.getValue().keySet()){
            if(!key.equals(cluster_id.getValue())){
                local_avg_dst = avg_dist_point_and_cluster(point, vectorToCluster, key);
                if (min_dist > local_avg_dst)
                    min_dist = local_avg_dst;
            }
        }

        return min_dist;
    }

    private static double min_avg_dist_aprox(Vector point, Broadcast<Map<Integer,Vector>> brCentroids , Integer cluster_id){

        double min_dist = Double.POSITIVE_INFINITY;
        double tmp_dist;
        for (Integer key : brCentroids.getValue().keySet()){
            if(!key.equals(cluster_id)){
                tmp_dist = avg_dist_point_and_cluster_aprox(point, brCentroids.getValue().get(key));
                if(tmp_dist < min_dist)
                    min_dist = tmp_dist;
            }
        }

        return min_dist;
    }


    /**
     * Calculates average distance a point with all the other points of cluster_id cluster
     * @param point the element i
     * @param vectorToCluster map of cluters with associated vectors
     * @param cluster_id id of the cluster of which i is part
     * @return avg distance from point to that cluster
     */
    private static double avg_dist_point_and_cluster(Vector point, Broadcast<Map<Integer, ArrayList<Vector>>> vectorToCluster, Broadcast<Integer> cluster_id){
        //new method
        ArrayList<Vector> vectors =  vectorToCluster.getValue().get(cluster_id.getValue()); //all vectors of the same cluster
        double dist = 0;
        for (Vector v : vectors)
            dist += Vectors.sqdist(point, v);
        return dist/vectors.size();
    }

    /**
     * Calculates average distance a point with all the other points of cluster_id cluster
     * @param point the element i
     * @param vectorToCluster map of cluters with associated vectors
     * @param cluster_id id of the cluster of which i is part
     * @return avg distance from point to that cluster
     */
    private static double avg_dist_point_and_cluster(Vector point, Broadcast<Map<Integer, ArrayList<Vector>>> vectorToCluster, Integer cluster_id){
        ArrayList<Vector> vectors =  vectorToCluster.getValue().get(cluster_id); //all vectors of the same cluster
        double dist = 0;
        for (Vector v : vectors)
            dist += Vectors.sqdist(point, v);
        return dist/vectors.size();
    }

    /**
     * Calculates distance from the point and the centroid of a cluster
     * @param point the element i
     * @param centroid the centroid of a cluster
     * @return distance from the point and the centroid
     */
    private static double avg_dist_point_and_cluster_aprox(Vector point, Vector centroid){
        return Vectors.sqdist(point,centroid);
    }

    /**
     *
     * @param a media delle distanze del punto con gli altri del suo cluster
     * @param b minima distanza tra il punto e le distanze medie coi punti di altri cluster
     * @return coefficiente di silhouette del singolo punto
     */
    private static double one_silhouetteCoef(double a, double b){
        double maxVal;

        if(a>b){
            maxVal = a;
        }else if(a<b){
            maxVal = b;
        }else{
            return 0;
        }
        return (b-a)/maxVal;
    }


    public static double random_silhouette_coefficient(KMeansModel my_cluster, JavaSparkContext sc, JavaRDD<Vector> reducted_dataset, JavaPairRDD<Integer, Vector> centroids){ //todo parameter

        //mappare il dataset ridotto secondo i cluster
        JavaRDD<Integer> clusterIndexes = my_cluster.predict(reducted_dataset);
        JavaPairRDD<Integer, Vector> vectorAndCluster = clusterIndexes.zip(reducted_dataset);

        JavaPairRDD<Integer,ArrayList<Vector>> clusteredVector = vectorAndCluster
                .mapValues((l)->{
                    ArrayList<Vector> list = new ArrayList<>(1);
                    list.add(l);
                    return list;
                })
                .reduceByKey((l1,l2)->{
                    l1.addAll(l2);
                    return l1;
                });

        //esegue il dataset preciso sul sample ridotto
        double sil_coef = silhouette_evaluation(sc, clusteredVector, centroids);
        //double sil_coef = silhouette_evaluation_aprox(sc, clusteredVector, centroids);

        return sil_coef;
    }

    /**
     *
     * @param my_cluster
     * @param sc
     * @param dataset
     * @param centroids
     * @param percentage
     * @return
     */
    public static double random_percent_silhouette_coefficient(KMeansModel my_cluster, JavaSparkContext sc, JavaRDD<Vector> dataset, JavaPairRDD<Integer, Vector> centroids, int percentage){ //todo check


        System.out.println("SIM: dentro al random percentage_silhouette coefficient con percentage = "+ percentage);
        //mappare il dataset ridotto secondo i cluster
        JavaRDD<Integer> clusterIndexes = my_cluster.predict(dataset);
        JavaPairRDD<Integer, Vector> vectorAndCluster = clusterIndexes.zip(dataset); //clustered vector totali

        System.out.println("SIM: Numero di vettori totali = " + vectorAndCluster.values().collect().size());

        //per ogni cluster tiro fuori la dimensione
        Map<Integer, Integer> cluster_dim = vectorAndCluster
                .mapValues((l) -> 1)
                .reduceByKey((l1, l2) -> l1 +l2 )
                .collectAsMap();

        System.out.println("SIM: cluster_dim = " + cluster_dim);

        Random rand = new Random(); //inizializzo il random

        JavaPairRDD<Integer, ArrayList<Vector>> reductClusteredVector = vectorAndCluster
                .filter(coppia -> {
                    int val = rand.nextInt(100);

                    if(val <= percentage){
                        System.out.println("SIM: percentuale true: " + val);
                        return true;
                    }else{
                        System.out.println("SIM: percentuale false: " + val);
                        return false;
                    }
                })
                .mapValues((l)->{
                    ArrayList<Vector> list = new ArrayList<>(1);
                    list.add(l);
                    return list;
                })
                .reduceByKey((l1,l2)->{
                    l1.addAll(l2);
                    return l1;
                }).cache();

        Map<Integer, ArrayList<Vector>>  pippo = reductClusteredVector.collectAsMap();
        System.out.println("SIM: Numero totale di cluster dopo la riduzione = "+ pippo.keySet().size() );
        for(int key : pippo.keySet()){
            System.out.println("SIM: cluster_number = " + key + " dimensione = " + pippo.get(key).size());
        }

        //esegue il dataset preciso sul sample ridotto
        double sil_coef = silhouette_evaluation(sc, reductClusteredVector, centroids);
        //double sil_coef = silhouette_evaluation_aprox(sc, clusteredVector, centroids);

        return sil_coef;
    }
}
