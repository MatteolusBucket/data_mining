package it.unipd.dei.dm1617.project;

import it.unipd.dei.dm1617.WikiPage;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.clustering.BisectingKMeans;
import org.apache.spark.mllib.clustering.BisectingKMeansModel;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.spark_project.guava.collect.ArrayListMultimap;
import org.spark_project.guava.collect.Multimap;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by root on 23/05/17.
 */
public class my_cluster {

    /**
     *
     * @param pagesAndVectors pair of wikipage and associated vector
     * @param MaxIt maximum number of iteration
     * @param numb_k number of cluster
     * @return a multimap with wikipage and associated cluster's id
     */
    public static Multimap<Object, WikiPage> my_kmeans(JavaPairRDD<WikiPage, org.apache.spark.mllib.linalg.Vector> pagesAndVectors, int MaxIt, int numb_k){
        System.out.println("CLUSTERING KMEANS");

        //execute kmeans cluster
        KMeans km = new KMeans().setK(numb_k).setMaxIterations(MaxIt);
        KMeansModel my_cluster = KMeans.train(pagesAndVectors.values().rdd(), km.getK(), km.getMaxIterations());
        return clustDataset(pagesAndVectors, my_cluster);
    }

    /**
     * return a pair wikipage and cluster id
     * @param my_cluster cluster center
     * @param pagesAndVectors pair of page and associated vector
     *
     * @return Multimap<Object, WikiPage>
     */
    private static Multimap<Object, WikiPage> clustDataset(JavaPairRDD<WikiPage, Vector> pagesAndVectors, KMeansModel my_cluster){

        //map pageAndVector in an iterable multimap
        Map<WikiPage, Vector> wiki2 =  pagesAndVectors.collectAsMap();

        //pair wikipage with its cluster number
        Multimap<Object, WikiPage> wikiToDataset = ArrayListMultimap.create();

        //scansiono tutte le pagine e le aggiunge alla mappa
        for(WikiPage page : wiki2.keySet()){
            wikiToDataset.put(my_cluster.predict(wiki2.get(page)), page);
        }

        //System.out.println("Elementi della mappa: " + wikiToDataset + "\n");
        if(false){
            System.out.println("OUTPUT DIAGNOSTICO");
            System.out.println("Numero di pagine per custer: " + wikiToDataset.keys());

            System.out.println("Inizio valutazione frazionamento");
        }

        return wikiToDataset;
    }



    public static KMeansModel my_kmeans_model(JavaPairRDD<WikiPage, org.apache.spark.mllib.linalg.Vector> pagesAndVectors, int MaxIt, int numb_k){
        System.out.println("MODEL CLUSTERING KMEANS");

        //execute kmeans cluster
        KMeans km = new KMeans().setK(numb_k).setMaxIterations(MaxIt);
        return KMeans.train(pagesAndVectors.values().rdd(), km.getK(), km.getMaxIterations());
    }

    /**
     *
     * @param pagesAndVectors pair of wikipage and associated vector
     * @param MaxIt maximum number of iteration
     * @param numb_k number of cluster
     * @return a multimap with wikipage and associated cluster's id
     *//*
    public static JavaPairRDD<Integer, ArrayList<WikiPage>> my_kmeans(JavaPairRDD<WikiPage, org.apache.spark.mllib.linalg.Vector> pagesAndVectors, int MaxIt, int numb_k){
        System.out.println("CLUSTERING KMEANS");

        //execute kmeans cluster
        KMeans km = new KMeans().setK(numb_k).setMaxIterations(MaxIt);
        KMeansModel my_cluster = KMeans.train(pagesAndVectors.values().rdd(), km.getK(), km.getMaxIterations());
        return clustDataset(pagesAndVectors, my_cluster);
    }

    /**
     * return a pair wikipage and cluster id
     * @param my_cluster cluster center
     * @param pagesAndVectors pair of page and associated vector
     *
     * @return Multimap<Object, WikiPage>


    public static JavaPairRDD<Integer, ArrayList<WikiPage>> clustDataset(JavaPairRDD<WikiPage, Vector> pagesAndVectors, KMeansModel my_cluster){

        JavaRDD<Integer> clusterIndexes = my_cluster.predict(pagesAndVectors.values());
        JavaPairRDD<Integer,WikiPage> pageAndCluster = clusterIndexes.zip(pagesAndVectors.keys());

        JavaPairRDD<Integer,ArrayList<WikiPage>> clusteredPages = pageAndCluster
                .mapValues((l)->{
                    ArrayList<WikiPage> list = new ArrayList<>(1);
                    list.add(l);
                    return list;
                })
                .reduceByKey((l1,l2)->{
                    l1.addAll(l2);
                    return l1;
                });

        return clusteredPages;
    }
    */


    public static BisectingKMeansModel my_bisectingkmeans_model(JavaPairRDD<WikiPage, Vector> pagesAndVectors, int MaxIt, int numb_k){
        System.out.println("MODEL CLUSTERING KMEANS");

        //execute kmeans cluster
        BisectingKMeans bkm = new BisectingKMeans().setK(numb_k).setMaxIterations(MaxIt);
        return bkm.run(pagesAndVectors.values());
    }


    public static JavaPairRDD<Integer, ArrayList<WikiPage>> my_bisecting_kmeans(JavaPairRDD<WikiPage, Vector> pagesAndVectors, int maxIt, int numb_k) {
        System.out.println("CLUSTERING HIERARCHICAL");

        //execute hierarchical cluster
        BisectingKMeans bkm = new BisectingKMeans().setK(numb_k).setMaxIterations(maxIt);
        BisectingKMeansModel my_cluster = bkm.run(pagesAndVectors.values());
        return clustDataset(pagesAndVectors, my_cluster);
    }

    private static JavaPairRDD<Integer, ArrayList<WikiPage>> clustDataset(JavaPairRDD<WikiPage, Vector> pagesAndVectors, BisectingKMeansModel my_cluster){

        JavaRDD<Integer> clusterIndexes = my_cluster.predict(pagesAndVectors.values());
        JavaPairRDD<Integer,WikiPage> pageAndCluster = clusterIndexes.zip(pagesAndVectors.keys());
        JavaPairRDD<Integer,ArrayList<WikiPage>> clusteredPages = pageAndCluster
                .mapValues((l)->{
                    ArrayList<WikiPage> list = new ArrayList<>(1);
                    list.add(l);
                    return list;
                })
                .reduceByKey((l1,l2)->{
                    l1.addAll(l2);
                    return l1;
                });

        if(false){
            System.out.println("OUTPUT DIAGNOSTICO");
            //System.out.println("Numero di pagine per custer: " + .keys());

            System.out.println("Inizio valutazione frazionamento");
        }

        return clusteredPages;
    }


    /**
     * return a pair vector and cluster id
     * @param my_cluster cluster center
     * @param pagesAndVectors pair of page and associated vector
     *
     * @return JavaPairRDD<Integer, ArrayList<Vector>>
     */
    public static JavaPairRDD<Integer, ArrayList<Vector>> clustVector(JavaPairRDD<WikiPage, Vector> pagesAndVectors, KMeansModel my_cluster){

        JavaRDD<Integer> clusterIndexes = my_cluster.predict(pagesAndVectors.values());
        JavaPairRDD<Integer, Vector> vectorAndCluster = clusterIndexes.zip(pagesAndVectors.values());

        JavaPairRDD<Integer,ArrayList<Vector>> clusteredVector = vectorAndCluster
                .mapValues((l)->{
                    ArrayList<Vector> list = new ArrayList<>(1);
                    list.add(l);
                    return list;
                })
                .reduceByKey((l1,l2)->{
                    l1.addAll(l2);
                    return l1;
                });

        return clusteredVector;
    }

    /**
     * Overloaded version for bisecting kmeans
     * @param pagesAndVectors pair of page and associated vector
     * @param my_cluster cluster center
     * @return JavaPairRDD<Integer, ArrayList<Vector>>
     */
    public static JavaPairRDD<Integer, ArrayList<Vector>> clustVector(JavaPairRDD<WikiPage, Vector> pagesAndVectors, BisectingKMeansModel my_cluster){

        JavaRDD<Integer> clusterIndexes = my_cluster.predict(pagesAndVectors.values());
        JavaPairRDD<Integer, Vector> vectorAndCluster = clusterIndexes.zip(pagesAndVectors.values());

        JavaPairRDD<Integer,ArrayList<Vector>> clusteredVector = vectorAndCluster
                .mapValues((l)->{
                    ArrayList<Vector> list = new ArrayList<>(1);
                    list.add(l);
                    return list;
                })
                .reduceByKey((l1,l2)->{
                    l1.addAll(l2);
                    return l1;
                });

        return clusteredVector; //todo controllare che sia giusto
    }

    /**
     * return a pair vector and cluster id without array list
     * @param my_cluster cluster center
     * @param pagesAndVectors pair of page and associated vector
     *
     * @return JavaPairRDD<Integer, Vector>
     */
    public static JavaPairRDD<Integer, Vector> clustVector2(JavaPairRDD<WikiPage, Vector> pagesAndVectors, KMeansModel my_cluster){

        JavaRDD<Integer> clusterIndexes = my_cluster.predict(pagesAndVectors.values());
        JavaPairRDD<Integer, Vector> vectorAndCluster = clusterIndexes.zip(pagesAndVectors.values());

        return vectorAndCluster; //todo controllare che sia giusto
    }

}
