package it.unipd.dei.dm1617.project;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import it.unipd.dei.dm1617.WikiPage;
import org.apache.spark.api.java.JavaPairRDD;
    import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.feature.Word2VecModel;
import org.apache.spark.mllib.linalg.Vector;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by matteo on 19/05/17.
 */
public class cluster {

    /*
     @param 1 = chiave valore del pages vector creato da w2vimport
     @param 2 = modello
     */
    public static double[] cluster1(JavaPairRDD<WikiPage, Vector> pagesAndVectors, Word2VecModel model, int MaxIt, int numb_k) {
        System.out.println("CLUSTERING KMEANS PARTITO");

        KMeans km = new KMeans().setK(numb_k).setMaxIterations(MaxIt);
        KMeansModel my_cluster = KMeans.train(pagesAndVectors.values().rdd(), km.getK(), km.getMaxIterations());
        double[] result_dati = new double[2];

        result_dati[0] = my_cluster.computeCost(pagesAndVectors.values().rdd());
        result_dati[1] = clustDataset(pagesAndVectors, my_cluster);

        return result_dati;
    }


    /*
     @param 1 dataset chiave valore pagine vettori
     @param 2 cluster
     */
    private static double clustDataset(JavaPairRDD<WikiPage, Vector> pagesAndVectors, KMeansModel my_cluster){
        System.out.println("PUNTO 21");

        //stampa tutti i centri pari al numero di K
        if(false) {
            System.out.println("Cluster centers:");
            for (Vector center : my_cluster.clusterCenters()) {
                System.out.println(" " + center);
            }
        }

        Multimap<Object, WikiPage> wikiToDataset = ArrayListMultimap.create();

        System.out.println("PUNTO 22");

        Map<WikiPage, Vector> wiki2 =  pagesAndVectors.collectAsMap();

        //scansiono tutte le pagine e le aggiunge alla mappa
        for(WikiPage page : wiki2.keySet()){
            wikiToDataset.put(my_cluster.predict(wiki2.get(page)), page);
        }

        //System.out.println("Elementi della mappa: " + wikiToDataset + "\n");
        if(true){
            System.out.println("OUTPUT DIAGNOSTICO");
            System.out.println("Numero di pagine per custer: " + wikiToDataset.keys());

            System.out.println("Inizio valutazione frazionamento");


        }
        return cluster_fraction_evaluation(wikiToDataset);
    }

    private static double cluster_fraction_evaluation(Multimap<Object, WikiPage> wikiToDataset){
        //scorre tutte le wikipage per ognuna mappa le categorie
        // nel dataset in cui e' stata mappata la pagina

        //mappa in cui la chiave e' la categoria e il valore e' il numero del cluster in cui e' stata mappata
        Multimap<String, Integer> cat_to_cluster = ArrayListMultimap.create();

        double fraction = 0;

        //valuto tutti i cluster
        for(Object cls_numb : wikiToDataset.keySet()) {

            //
            // System.out.println("\nvaluto il cluster numero: " + cls_numb);

            //per ogni pagina associata a quel cluster
            for(WikiPage page : wikiToDataset.get(cls_numb)){
                //System.out.println("\nvaluto la pagina numero: " + page.getId());
                //per ogni categoria associata a quella pagina
                for(String cat : page.getCategories()){
                    cat_to_cluster.put(cat, (Integer) cls_numb);
                    //System.out.println("\nvaluto la categoria: " + cat);
                }
            }
        }

        //per ogni categoria vado a vedere il numero di cluster diversi a cui e' associata
        for(String cat : cat_to_cluster.keySet()){
            //System.out.println("\nvaluto la categoria: " + cat);

            Map<Integer, Integer> statistic = new HashMap<>();
            for (Integer element : cat_to_cluster.get(cat)){
                Integer count = 0;
                if (statistic.containsKey(element)){
                    count = statistic.get(element);
                }
                count++;
                statistic.put(element,count);
            }

            fraction = fraction + (1 / statistic.size());

            //System.out.println("\nStatistic vector: " + statistic);
        }

        int numb_categories = cat_to_cluster.keySet().size();
        fraction = fraction / numb_categories;
        //System.out.println("Numero di categorie = " + numb_categories + ", percentuale di frazionamento = " + fraction);
        return fraction;
    }

}
